FROM alpine as downloader

ARG OPENSHIFT_VERSION
ARG OPENSHIFT_FILENAME

RUN apk add curl
RUN curl -L https://github.com/openshift/origin/releases/download/v$OPENSHIFT_VERSION/$OPENSHIFT_FILENAME | tar xvz && \
    cp openshift-origin-client-tools*/oc ./

FROM debian:9-slim
MAINTAINER mikk150 <mikk150@gmail.com>

RUN apt-get update && apt-get install -y ca-certificates && \
    apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

COPY --from=downloader oc /bin